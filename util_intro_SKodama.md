# 児玉聡『功利主義入門――はじめての倫理学』 (2012)

## 第1章 倫理と倫理学についての素朴な疑問

### 倫理的相対主義

「倫理は時代や場所によって異なり，絶対的に正しい答えなどない」という立場。
これに対して三つの指摘ができる。

1. 一見して相対的に見えるルールであっても，よく考えればより深い共通のルールがその基礎にあると理解できることがある。
2. 人間の生物的条件や社会的条件が大きく変わらないかぎり根本的に変化しない普遍的なルールがある。
3. 仮に相対主義が正しいとすれば，あらゆる「〜べき」という主張が不可能になる。

### 神なしの倫理はありうるのか

神の存在を信じない人間は，倫理的ルールは人間が社会生活を長く営む間に人々が徐々に作り出していったものだと考える。
そのため根本的な変更は起こりにくいが，ある程度の変更はありうる。

人間は自分の利益と倫理的ルールが衝突する際にルールを破ってしまいがちである。そこで神が要請される。
しかし神による動機付けがなくともわれわれはある程度までは倫理的ルールに則って行動する動機を持つ。
たとえば，良いことをすれば誉められ，悪いことをすれば注意されたり仲間はずれにされたりする。
もちろん社会一般が堕落していれば多くの人は倫理を守らないかもしれない。
しかし，そのために神を持ちださなくとも，道徳教育を行ったり，倫理的ルールが必要な理由を考えさせたりすることで，動機を与えられる。

### 利己主義について

倫理的ルールの多くは利他的に行為することを勧めるが，人間はつねに利己的に行為するのであり，利他的になれと命じても仕方ないとする立場。
たとえば，ボランティアのような一見利他的な行為も，自分がそれをやりたいからやっているのであり，倫理的であるとはいえないという考え。

このような考え方に対しては二つの答え方がある。

1. 倫理において動機はあまり重要ではないという立場からの答え。
2. 人間はつねに利己的に行為するというのは誤った考えであるという答え。

1.においては大切なのは行為の際の動機の善し悪しではなく，倫理的な行為がなされるかどうかである。[要は帰結主義ということか。]
このような立場からすれば，人間が仮につねに利己的であっても倫理的行為をするよう仕向けることができる。

2.について，ホッブズがまさにそう主張した。
たとえば困っている人を見て助けるのも，それを見るのが苦痛であるから助けるのだという。
このように「利己的」の語が再定義されることで，「利他的」の対義語としての「利己的」では最早なく，新たな別の言葉となってしまう。
この定義を受け入れるとあらゆる行為が利己的になるが，それでも困った人を助けることは依然可能であるから，そうした行為を勧める意味はあるといえる。
すると最初の「人間はつねに利己的に行為するのであり，利他的になれと命じても仕方ない」という意見は間違いになる。

### 「自然に従う」だけでよいのか

「倫理は大事だが，倫理理論などを学ぶ必要はなく，自然に従っていればよい」という考え方がある。
これに最も明快で徹底した批判をしているのがJ・S・ミルである。
ミルによれば，「自然に従うべきだ」という主張は，

* どう行為すれば「自然に従う」ことになるのかが曖昧で無意味である
* たとえ意味があったとしても，そのような基準に従うことは「不合理であると同時に不道徳」
	* 不合理だというのは，「人間の行なうあらゆる行為は，自然のあるがままのあり方を変更するものだからであり，すべての有用な行為はそれを改善するものだから」
	* 不道徳だというのは，「自然現象は，人間が行なうなら憎悪の対象になって当たり前の出来事で満ち溢れており，事物の自然なあり方をまねしようとする人物がいたならば，その人はみなから最悪の人物と見なされるだろうから」

自然の残酷な面を考慮に入れず「自然に従うべき」と主張する人は，

1. 自分が日頃から慣れ親しんでいることを「自然なこと」だと考えている。
2. 自然が持つ諸側面の中から，自分に都合のよいことを「自然だ」と呼んでいる。

### 倫理学は非倫理的か

倫理学における思考実験には人が死ぬものが多く，それ自体が倫理的ではないのではないか。
倫理的ルールを教え込む道徳教育の段階ではなく，それを批判的に検討するのが倫理学であり，そこには二つの関心がある。

1. 理論的関心。倫理的ジレンマ状況における自らの思考を批判的に検討し，そんな状況にも対応しうる倫理的理論を作り出す必要性から，扱いやすい抽象的で架空のケースを用いる。
2. 実践的関心。倫理的ジレンマ状況は実際に起こるので，普段からどうするかを批判的に考え準備しておく。

## 第2章 功利主義とは何か

功利主義はベンタムによって最初に定式化された。何をなすべきかの指針は「功利性の原理」すなわち「最大多数の最大幸福」だと主張した。

### 功利性の原理とそれに対立する二つの原理

ベンタムが検討した正・不正の基準としては，以下のとおり。(2.と3.はベンタムが批判した考え方。)

1. 功利性の原理
2. 禁欲主義の原理
3. 共感・反感の原理

2.は1.とは真逆の原理で，苦痛が善であり快楽が悪だという考え方。
ベンタムによると，古代のストア派や一部の宗教家が主張。

3.は，正しい行為とは自分の気に入った行為のことであり，不正な行為とは自分が気に入らない行為のことであるという考え方。
ベンタムによると，彼以前のほとんどの哲学者はこの考え方で，「自然の法」とか「良心」とか「永遠不変の真理」とかいう言葉でもっともらしく見せかけていた。
しかし，こういう考え方を持ち出しても議論のしようがなく，結局多数者が少数者に，権力者が社会的弱者に自分たちの考えを押しつけることになると批判した。

### 功利計算

ベンタムによれば，何をすればよいかを考えるにあたって，快苦の量をきちんと計算しなければならない。

快苦にはその強弱や長短，得られるまでの時間があり，とくに将来の快苦については確実性も問題になる。
さらに，それが後で快をともなうのか苦痛をともなうのかという考慮や，ある人に快をもたらす行為が他人に快あるいは苦痛をもたらす傾向があるかどうかの考慮も必要。

さらにベンタムは種類の分類も行なう。快は十四種類，苦痛は十二種類ある。

また，計算の際には「各人を一人として数え，誰もそれ以上には数えない」としている。

### なぜ功利主義に従うのか

ベンタムは，快苦が生じる源泉には四つあるという。
(サンクションとは報償や制裁のように特定の行動を取るようあるいは取らぬよう，われわれを動機づけるもののこと。)

1. 自然的サンクション。自然の経過によって発生する快苦のこと。
2. 政治的サンクション。政治的・法的な強制力によって生じるもの。
3. 民衆的サンクション。周囲の人の白い目や仲間外れによって生じるもの。
4. 宗教的サンクション。宗教的な源泉によるもの。

1.は快苦が自然的に生じる場合なのに対して，2.と3.は人為的に生じる場合といえる。
ベンタムは，立法者は主に政治的サンクションを用いて，人々を功利原理にかなう仕方で行為するよう統制する必要があると考えた。

### 功利主義の三つの特徴

1. 帰結主義。行為の正しさを評価するには，行為の帰結を評価することが重要であるという立場。
2. 幸福主義。行為が人々の幸福に与える影響こそが倫理的に重要な帰結であるという立場。
3. 総和最大化。一個人の幸福の最大化ではなく，人々の幸福の総和の最大化を目指す立場。

1.に対して非帰結主義の立場がある。
たとえば，「共感・反感の原理」であったり，行為の正しさを動機の善し悪しで評価する立場や，帰結がどうであれ各人の権利が大事であるという立場など。

2.は幸福にのみ，それ自体で価値があるという「内在的価値」を認め，それ以外の自由や真理などには幸福のための手段としての「道具的価値」しか認めないという立場。

3.は前提として，一人一人が等しい配慮を受けねばならない(「各人を一人として数え，誰もそれ以上には数えない」)。
総和最大化から明らかなように功利主義は利己主義ではない。この特徴に対しては，「功利主義には分配的正義の配慮がない」という重要な批判がある(第5章)。

## 第3章 功利主義者を批判する

ベンタムが「各人を一人として数え，誰もそれ以上には数えない」と言ったように，公平性を重視する功利主義者からすれば，身内びいきには批判的にならざるをえない。
たとえばゴドウィンは，「人をひいきしない」のが正義の原理だと述べ，自分や自分の家族を無条件で優先することを認めてはならないと主張する。

常識的には，自分の家族に対する愛情というのは無条件なものであり，家族以外の人よりも大切にするべきだ。
実際，功利主義者たちは「功利主義は常識はずれの結論を支持する」という批判を受けてきた。

## 第4章 洗練された功利主義

### ゴドウィンの修正

正義の本質が人々の公平な扱いにあることについてはゴドウィンは考えを変えなかった。
しかし，家族への偏った愛は不偏性を本質とする正義にとって邪魔になるので不要だという考えを改め，むしろ家族関係の中で愛情を育む人の方が身近な人をより幸福にできて功利主義的に見ても望ましく，それで他人の幸福に対する感受性が高められると他の人々の幸福にも関心を持つ立派な功利主義者になれる可能性があると考えるようになった。
ただし，ゴドウィンは「偏愛」を無条件に認めるわけではなく，適度でなければならないとし，どのくらいが適度であるかは，公平性の観点から評価が必要とした。

### 規則や義務の重視

公平性の観点が最も重要という考えは変化していないという意味でゴドウィンは依然として功利主義者であり，この立場は現代の功利主義者の多くが取る立場とほぼ同じである。
現代の功利主義者の多くも家族や友人に対する義務，約束を守る義務，嘘をつかない義務などが功利主義的に考えて重要であることを認めている。

ただし，あらゆる義務は，それをいかなる状況においても守ろうとすると，行き過ぎになることがある。
たとえばカント批判の文脈でよく出される例が，友だちを殺人者から匿う際に嘘をつくことが許されるかという例である。
厳格なカントはこの場合においてでも嘘をついてはいけないという。

これに対して現代の功利主義は二つの点で洗練されている。

1. 直接功利主義の立場ではなく，間接功利主義の立場でよいという点。
2. 行為功利主義の立場ではなく，規則功利主義の立場でよいという点。

1.からいえるのは，ひたすら最大多数の最大幸福のことばかり考えずとも，功利主義的に行為できるということである。
義務やさまざまな道徳的規則を考慮しながら行為していれば，結果的に功利主義的に行為することになるという。
こうした，逐一功利原理を用いて意思決定する必要はないという考えが，間接功利主義である。

2.からいえるのは，義務の重要性を認めつつも，行き過ぎがないように功利主義の観点からチェックする必要があるということである。
こうしたさまざまな道徳的規則や義務を社会全体の幸福に貢献するかで評価し，認められた規則や義務を二次的な規則として採用する立場が，規則功利主義である。
対して個々の行為について功利主義的な評価を下さなければならないという立場が，行為功利主義である。

### ミルの他者危害原則

J・S・ミルの他者危害原理は，規則功利主義者の支持する「二次的な規則」として理解できる。
行為功利主義の立場からすれば，個々のケースを功利主義的に判断した場合，個人の自由を制限した方が社会全体の幸福にとってよいこともあると考えられる。
しかしミルは，他人に危害を与えないかぎりで個人の自由を保障するという規則を採用した方が，個人の自由に絶えず介入しようとする社会より長期的には得られる全体の幸福は大きいと考えた。

### わら人形攻撃

一般に功利主義の批判者は，「行為功利主義」かつ「直接功利主義」であるような立場を念頭に批判をすることが多い。
しかし現代の功利主義者の多くは，大なり小なり「規則功利主義」と「間接功利主義」の立場を取っている。
彼らは普段，一般的な道徳的義務や規則に従っており，功利主義的思考は表にほとんど出てこない。

ただし，功利主義者が功利主義的にふるまう状況もある。
すなわち，常識的に従っている道徳的規則や義務が功利主義的に見て望ましくない場合がそれである。

### 公平性と「道徳的に重要な違い」

「道徳的に重要な違い」とは，その違いがあれば異なる仕方で扱うことが許され，なければ等しく扱うことが要求されるような違いのことである。

何が「道徳的に重要な違い」であるかという問いは公平性を重んじる功利主義にとって常に重要であり続けてきた。
この問いを通して，たとえばベンタムは同性愛の合法化や動物の虐待の禁止を主張し，ミルは女性参政権を主張した。
現代においても，ピーター・シンガーは貧しい国の人々を助けるべきだと主張し，動物の幸福についてもベンタムより大胆かつ実践的に論じた。

「最大多数の最大幸福」を言うとき，「最大多数」には誰が入るのか。
公平性を重視する功利主義は，われわれの視野を広げることを要求する。
その線引きは本当に道徳的に重要な違いに基づいているのかを問う。

## 第5章 公共政策と功利主義的思考

### 歴史的背景

功利主義が最初に提唱されたのは産業革命中の英国で，新しい時代にふさわしい社会改革を行なうべく提示されたものだった。
今日「最大多数の最大幸福」というスローガンは，ともすると「少数派の犠牲の上に多数派が幸福になるための思想」と理解されがちだが，元々は社会的弱者の幸福にも等しく配慮すべきと主張する立場だった。

### 現代の公共政策における功利主義的思考

* 菅元首相の「最小不幸社会」(カール・ポパーの「消極的功利主義」に近い)
* トリアージ
* 津波てんでんこ

### 功利主義と分配的正義

ジョン・ロールズは『正義論』の中で「功利主義は人格の個別性を無視する」と批判した。
つまり，人々の幸福を総和して最大化することを目指す功利主義においては，多数者の幸福のために一部の少数者が犠牲になっても構わないと判断される。

これに対して功利主義の側からはさしあたり二つの応答が考えられる。

1. そのような社会は長期的に見れば全体の幸福の最大化にはつながらないと主張する。
2. 政策を作るさいに指針となる二次的な規則を作る(つまり「規則功利主義」)。

2.についてはベンタムもミルも功利主義的政策を実施するさいの二次的な規則が必要だと考えていた(ミルの他者危害原理など)。

### 功利主義と自由主義

以上では，功利主義はほとんどの場合においてロックやロールズの自由主義とあまり違わないように思える。
では両者の違いはなにか。

ロックの自由主義では個人の自由や権利は自然権として生まれながらに持っており，ミルの自由主義ではそれらが認められるのはあくまで社会全体の幸福を最大化するからである。
さて，自然権思想には二つの問題がある。

1. 自然権の根拠を示すのが難しいということ。
2. 自然権を認める対象の範囲が不明確で，その適切な範囲についての議論が難しいということ。

まとめると，功利主義者は自由主義を擁護するが，その際に次の点に注意すべきだと考えている。

1. 目的手段関係。個人の自由の保障はあくまで個人および社会の幸福のための手段であり，それ自体を金科玉条にしてはいけない。
2. 実証性。功利主義においては，複数の政策のなかから，帰結がより優れたものを選ぶべきである。
3. 包摂性。政策を実行する上で，幸福を無視されている存在はないか。

### 功利主義の二つの顔

公共政策における功利主義を自由主義的なものとして理解して説明してきた。
しかし，功利主義は伝統的には人権を無視する反自由主義器な理論と考えられる傾向にあった。
すなわち権威主義的と自由主義的という二つの理解があった。
そこで，前者を象徴する存在としてエドウィン・チャドウィック，後者を代表する存在としてJ・S・ミルを「公衆衛生」というテーマで扱う。

### 「公衆衛生」とは

公衆衛生は英語でPublic HealthあるいはPublic's Healthである。
たとえば，予防接種やがん検診のように，人々が病気にならないよう予防的な介入を行なうことは公衆衛生活動である。
公衆衛生は「みんなの健康」を守るために健康な個人の生活に介入するという意味で倫理学や政治哲学的にとても興味深い領域だが，あまり認知されておらず，規範理論の観点から論じられることが少ない。

### 公衆衛生と功利主義

産業革命期には都市人口の増大と不潔な衛生環境が原因でコレラや結核，チフスなどが流行した。
さらに労働者の「道徳的退廃」も問題となった。
工場法や救貧法改正，公衆衛生法を代表とする十九世紀前半の立法は，産業革命によって生じたこうした社会問題に対応するためになされた。
これらの動きに大きく関わっていたのがチャドウィックである。

### チャドウィック

ベンタムは市民の幸福を最大限に実現する政府を作るために，『憲法典』で「保健省」の創設を提案し，中央集権による効率的な公衆衛生のビジョンを提示した。
そのなかで，チャドウィックはベンタムの公衆衛生に関する思想を行政官として実現しようとした人だったと言える。

チャドウィックの業績として特に知られているのが『大英帝国における労働人口集団の衛生状態』という報告書だ。
本報告書では，労働者の劣悪な住環境や労働環境が，高い疾病率や死亡率につながっており，大きな経済的損失を生み出すとともに道徳的退廃を引き起していることが論じられている。
また，現行の教区任せの公衆衛生行政ではなく，中央集権に基づく標準化された制度へと改革する必要性が説かれている。
チャドウィックは科学的知識を持った専門家による統治，中央による地方政府の統制，環境が人の健康に影響を与えるという衛生思想の三つの原則が重要だと考え，これらの原則に基づいて公衆衛生行政を行なおうとした。
またチャドウィックは公衆衛生活動で重要なのは医学ではなく工学だと考えていた点でも特徴的だった。
公衆衛生上の問題は工学的手法あるいは都市設計によって解決し，医師は病気になった人の治療に専念すればよい，という基本的発想を持っていた。

しかし実際には彼らの公衆衛生行政はうまく行かず，労働者の健康状態の改善や疾病予防を目的とした中央政府から地方当局への介入は，権威主義的だとして批判されただけでなく，私有財産や個人の自由に対する不当な介入だと批判された。

よかれあしかれ，チャドウィックの公衆衛生活動は，功利主義の一つの志向，つまり権威主義的でパターナリスティックな側面を示していたと思われる。

### J・S・ミル

ミルはチャドウィックの公衆衛生活動を高く評価していたが，少なくとも次の二つの点でチャドウィックとは対照的な考えを持っていた。

1. 中央集権や官僚制が人々の精神に影響を及ぼすことを強く批判していた。「能率と矛盾しないかぎりでの権力の最大限の分散，しかし可能な最大限の情報の集中化とそれの中央からの拡散」があるべき関係だと考えていた。
2. 個人の利益は当人自身が一番よく配慮することができると考えていた。公衆衛生活動もパターナリズムではなく，他者への危害の防止という根拠に基づいて行なうべきだと考えていた。

これは，功利主義のもう一つの側面である，自由主義的志向を反映したものと言える。

今日では，権威主義やパターナリズムが不人気であることもあり，チャドウィック流の権威主義的なものではなく，ミルのように自由主義を基礎付けるものとして功利主義を理解する立場の方が現代では主流となっている。
しかし，ミルの立場だとあまりに個人の自由を尊重しすぎており，現在の公衆衛生活動の多くを正当化することができなくなる可能性があり，公共政策としての功利主義はこれを何らかの形で「乗り越える」必要があると思われる。
そこで，以下で現在の公衆衛生の倫理学とその課題について素描し，筆者の考える方向性を簡単に示す。

### 公衆衛生の倫理学

英米では二〇〇〇年前後から公衆衛生の倫理的側面に注目が集まっている。その理由として，次の二点が挙がる。

1. 感染症に対する関心の復活。個人の自由が最大限尊重される自由主義社会で，強制入院や隔離措置，感染者の追跡調査や特定集団へのワクチン接種の義務化のような個人の自由の制限はどこまで正当化されるのか。
2. 医学研究の進歩による考え方の変化。たとえば「成人病」は「早期発見・早期治療」が主な対策だったが，「生活習慣病」となり健康増進活動が重視されるようになって個人のライフスタイルに干渉するようになった。ここで，どこまで病気の予防や健康増進といった目的のために個人の自由の制限が許されるのかという問いが生じる。

このように感染症対策と健康増進活動の進展に伴い，公衆衛生活動は以前にも増して個人の自由と衝突する可能性が出てきたため，公衆衛生の倫理学という領域が生まれてきた。

### 介入はどこまで許されるか

ミルの他者危害原理に従えば，他人に危害を加えない限り，健康に関わる個人の行動は自由だというものだろう。
だが，そうすると，現在行なわれている多くの公衆衛生活動は実施できなくなる。
たとえば，ミルの立場だと，麻薬を使用している人が他者に危害を加えない限りは，注意や説得を試みることはできるものの，薬物使用を禁止したり罰したりすることはできないことになる。

このように，ミルの立場は公衆衛生という政府の重要な仕事に関しては，大きな足枷となりうる。
このミルの立場をどう乗り越えるかが，当面のところ公衆衛生の倫理学の最大の課題となっている。

さてそこで，筆者はミルの自由主義的な公衆衛生活動を修正することにより，現代の公衆衛生活動を基礎付ける規範理論を提示したい。

### 人間はどこまで合理的か

近年注目を集めている政治哲学的立場に，リバタリアン・パターナリズムがある。
これは，政府は人々が自らの最善の利益を追求できるように配慮するが，あくまで強制はせず，各人が異なる選択肢を選べる自由を保障するというものだ。
セーラーとサンスティーンがこれを「ナッジ[nudge]」と呼んだが，ナッジはある行為を強制するのではなくそれを選ぶようにうまく誘導するというイメージだ。

リバタリアン・パターナリズムは一見ミルの自由主義の立場に近いが，ミルには見られなかった興味深い視点がある。
リバタリアン・パターナリズムは，「人間はあまり合理的に行動しない」という仮定から出発している。
とりわけこの傾向が顕著なのは健康行動である。
われわれが健康行動において不合理な行動をするのは，われわれが現在の快苦を過大評価する傾向にある(現在バイアス)ため，そしてわれわれの理性ではなく情動に働きかける宣伝によって，あまり考えることなしにそれに影響を受けた飲食習慣をわれわれが形成しているためだろう。

つまり，ミルの合理的な人間像に反して，われわれの多くは欲求が不安定で外的な影響に左右されやすい存在であり，しばしば不合理に行為するということだ。
H・L・A・ハートはある程度まではパターナリスティックな規制をして，望ましくない選択肢を選べないようにした方が，結果的に人々は合理的に行為できるだろうと考える。
一方，リバタリアン・パターナリズムでは，人々がより健康的な選択肢を意識的に選ぼうとしなくてもそうできるように環境の方を変更しようとする。

では，功利主義者がこうした不合理性を考慮に入れるとどうなるだろうか。
功利主義はリバタリアン・パターナリズムの考えの多くをほとんどそのまま受け入れることができるだろう。
しかしこの立場ではミルと同様，法によるパターナリズムをよしとしないので，公衆衛生活動は大変やりにくくなるだろう。
一方で功利主義は，ハートの言うようにある程度まではパターナリズムを受け入れることもできるだろう。
ただし，チャドウィック流の権威主義的な公衆衛生に戻るのは行き過ぎである。
真理はその中間にありそうだ。

近年，英国の公衆衛生の議論において，介入を強制の度合いに応じて七つのレベルに分ける，「公衆衛生的介入の階梯」が注目を浴びている。[(The Nuffield Council, *Public health: Ethical Issues*)](http://nuffieldbioethics.org/wp-content/uploads/2014/07/Public-health-ethical-issues.pdf)
功利主義はこのような知見を積極的に採用し，個人の自由の制限は最小限にしつつ，より効果的な公衆衛生活動を模索することができるだろう。

### 喫煙規制のケース

たばこによる副流煙が周囲にいる人間のさまざまな病気のリスクを高めるという科学的知見が大筋で正しいとしよう。
するとこれは他者危害にあたり，ミル流の自由主義でも当然,公共空間における分煙や全面禁煙などの規制の対象となるだろう。

より難しいのは，たとえば一人でいる自宅の部屋で行なう喫煙や，全面禁煙ならぬ「全面喫煙」の喫茶店の規制だ。
仮に，その喫茶店の店員がみな受動喫煙の害を承知の上で働いていて，客もそれを承知の上で店に入ってくるなら，政府は規制すべきだろうか。

ミルならば，喫煙者が合理的判断に基づいているかどうかだけを問題にするだろうが，上述した，人間の不合理性をより重視する立場だと，もう一歩進んだ規制も視野に入るだろう。
仮に喫煙者が自発的に行為していると仮定した場合でも，長期的な自己利益を考慮した合理的判断ではなく無意識に喫煙を選んでいるということになれば，自販機やライター，灰皿を目に付きにくい場所に置いて誘因を減らす「ナッジ」的戦略や，場合によってはたばこ税増税などで衝動に抗う動機を作り出すことも正当化されうる。

ただし，功利主義においては，強制は苦痛を生み出す可能性が高いためできるだけ避けるべきである。
したがって，生産の禁止や喫煙の違法化などの強制は長所短所の十分な考慮のうえでしか正当化されない。
むしろ，先の「階梯」のようにより穏健な措置を先に検討することが功利主義的にも望ましい。
また，ミルが地方自治を強調していたように，人々の自発的な参加と協力によって運営することも功利主義の立場からは大切だ。
みんなの健康は，自分たちで守る方がよりよい結果をもたらすと思われるからだ。

筆者は功利主義が極端な自由主義と権威主義の間を行く理論を提供しうると考えているが，この立場の展開は今後の課題と言える。

## 第6章 幸福について

### 低調な幸福論

倫理学は幸福について研究する学問だと思われがちである。
しかし今日では，幸福論あるいは「よい人生とは何か」という問いが正面から論じられることはほとんどない。

その理由は二つ。メタ倫理学の流行とロールズ以降の自由主義における不介入主義だ。
二十世紀の特に英米圏で言語分析が盛んになり，メタ倫理学が生まれたが，そのために倫理学者たちは積極的に社会問題や「実際にどう生きるべきか」といった問題に正面から答えなくなった。
このような禁欲主義に風穴を開けたのがロールズの『正義論』だと言われるが，ここでも幸福論への逆風は変わらない。
たしかに「社会がどうあるべきか」という規範的な問いが正面から問われていたが，ロールズの支持する自由主義は個人の生き方について立ち入らない。
よってこの二つ，二十世紀の英米倫理学における禁欲主義，ロールズ以降の自由主義における各人の人生観に関する不介入主義が幸福論にとって大きな足枷となっている。

しかし，功利主義は「社会全体の幸福を最大化せよ」と主張する立場であり，幸福とは何かについてある程度の考えを示さねばならない。

### 「幸福とは何か」という問い

幸福そのものを直接計る機械はまだない。
もちろん，人々に幸福かと尋ね，主観的幸福感を調べることができる。
しかし，この方法にも問題がいくつかある。
まず，本人が正直に答えているかどうかがわからない。
また，仮に調査に協力した人全員が正直に答えているとしても，客観的に見て幸福と言えるとは限らないという問題がある。
そのため，必ずしも信頼がおけるものではない。

一方，経済や政治の分野では長い間，GNPやGDPが一国の幸福メーターになると考えられてきた。
しかし，先進国では一九六〇年代以降，GDPの増大が国民の幸福の増大につながっていないという指摘がなされてきた。
そこで最近，政府が政策の方向性を決める際の参考にするために，客観的な指標を組み合わせて，より信頼のおける幸福メーターを作ろうという試みがなされている。
しかし，客観的指標は幸福とどのような関係があるのか，その指標が幸福の要素だとするとそれに共通する性質は一体何なのかという問題が生じる。

十分な所得や富を持つこと，心身ともに健康であること，家族や友人を持つことは，幸福に役立ちそうなことは容易に想像がつく。
しかし，それらが共通して持つ，われわれの幸福に与える影響とはいったい何なのか。
これが「幸福とは何か」という問いで問われていることなのだ。

### ベンタムやミルの快楽説

この問いに対する一つの答えは，「所得や健康や家族などが幸福に役立つというとき，それらすべてに共通しているのは，われわれに快楽をもたらすということだ」というものだ。
ベンタムは苦痛そのものを善いとする禁欲主義に反対し，快楽そのものはすべて善だと述べた。
そして幸福とは快楽のこと，あるいは苦痛が存在しないことであり，不幸とは苦痛のこと，もしくは快楽が欠如していることだと主張した。
ただし彼はどの快楽が幸福につながるかということに関しては，基本的に無頓着だった。
ベンタムは，その強度や持続性等々において等しいのであれば，どちらがより優れた快楽かは一概にはいえないという。
これは，先述した自由主義の不介入主義の発想に近い。

一方ミルは，快楽には質の違いがあると述べた。
これはベンタムを直接批判したものではなく，功利主義は人々が低級な「ブタの快楽」を追求することを支持しているという批判に答えたものだった。

> 満足したブタよりも不満足な人間の方がよい。満足した愚か者よりも不満足なソクラテスの方がよい。

ミルはこのように述べ，精神的な快楽を高級な快，身体的な快楽を低級な快とした。
そしてこれらの違いは，両者を経験した人には容易に判定できると考えた。
ただしミルは，耄碌した人などは易きにつきがいで誤った選択をする可能性があるの言っている。
また，意見が割れる場合には多数決によって決めなければならないとも考えていたようだ。

脱線であるが，ミルは快楽の質の話はしたが，苦痛の質という話はしなかった。
しかし，たとえば身体的な苦痛と精神的な苦痛のどちらが「高級な苦痛」なのか，また，高級な苦痛と低級な苦痛のどちらをより避けるべきなのだろうか。
さらに，われわれは高級な快楽を味わえる人間になるべきだとミルは考えていたようだが，高級な苦痛を感じられる人間にもなるべきなのだろうか。

さて，快楽に質があり，質の違いは両者を経験した人にはわかるというミルの考えに従えば，より高級な快楽というのがリスト化され，それを追求するのが幸福につながることになる。
だが，ミルはこのようなリストを作成することはしなかった。
ミルはパターナリズムを強く批判し，各人は自由に「人生の実験」を行なうべきだと考えた。
それを通じて，自分やあとに続く人々のためによりよい生き方を見つけるべきだと考えたからである。

### 快苦の定義は可能か

ベンタムやミルのいわゆる古典的功利主義では，幸福＝快楽，不幸＝苦痛と考えられた。
しかしこれにはいくつか問題がある。

一つに，快楽とはいったい何なのかがよくわからないということだ。
これは「幸福とは何か」という問いと同じ問題で，さまざまな快楽に共通する性質とはいったいどのようなものかがよくわからない。

たとえば，何かの目的のために禁酒をするとして，その目的を達成した際の快楽と酒を我慢することの苦痛とは比較可能だろうか。
われわれの行為すべてに共通する「快楽」を定義するのは難しく，仮にできても通常の意味とは異なる内容空疎なものになっている可能性がある。

### 機械や薬で幸福になる？

二つめは，たとえば「快楽とは，望ましい意識状態のことである」と定義できたとして，われわれはそのような快楽の追求を本当に幸福だと思っているだろうかという問題だ。

ノージックによる有名な「[経験機械](https://ja.wikipedia.org/wiki/%E5%BF%AB%E6%A5%BD%E4%B8%BB%E7%BE%A9#.E5.BF.AB.E6.A5.BD.E6.A9.9F.E6.A2.B0)」という思考実験がある。
永遠に最高度の快楽を与えられ続ける機械につながれることが幸福だといえるだろうか。
あるいは，オルダス・ハクスリーの『すばらしい新世界』における「ソーマ」という薬も同じである。
つまり，われわれは快楽を感じているだけでは必ずしも幸福とは言えないのではないかということである。

### 幸福＝欲求の満足か

次に，幸福とは欲求が満たされることだという考え方について考察する。
上述のとおり，快楽は主観的な経験で測定が難しい。
そこで，人々が欲し選択するという事実を幸福の基準にしようという発想が出てくる。
幸福の要素は結局，われわれが欲しているからこそわれわれの幸福に影響を及ぼすのではないかというわけだ。

選好の充足，平たく言えば「望みが叶う」ということを幸福として理解すると，功利主義的に正しい行為とは，人々の選好や欲求を最大限に充足させる行為ということになる。
この立場を「選好功利主義」という。

しかしこの考え方にも問題がある。
まず，選好充足が本当に幸福につながるのかという問題がある。
たとえば，知性を誇る哲学者が年を取ってそれを失うことをひどく恐れており，もしアルツハイマー病になったらなるべく早く死なせてほしいと言っていた。
果たしてその人は実際にアルツハイマー病を発症した。
ある日肺炎を患ったが適切な治療を受ければ完治するという状況で，かつての選好を満たすために治療をせざるべきか，快苦を重視して治療をすべきか，その人にとっての幸福はどちらであろうか。

また，ノーベル賞受賞を楽しみにしていた研究者が，受賞発表の数日前に死亡していた時，その人は既に死んでいたので受賞による快楽や満足は感じられないが，選好は満たされたことになる。
この場合，彼は幸福になったと言うべきだろうか。

このように，ある人がもつ選好は，その人が知らないところで，また死んだ後でも充足されうる。
しかし，アルツハイマー病のケースで選好を満たすことが幸福になるかは議論の余地があり，また，ノーベル賞のケースで死んだ後に選好が充足された場合，本人が幸せになったと言えるかはさらに疑わしい。
そうすると，われわれがもつすべての選好を充足させることが幸福につながるわけではなく，一部の選好を満たすことが重要だといえる。
ところが，この「幸福につながる一部の選好」を定義することはかなり難しい。

### 適応的選好の形成

より現実的で深刻な問題にアマルティア・センによる「適応的選好の形成」という問題がある。
非常に制限された環境や構造的な差別が存在する環境に育ってきた人は，その環境に適応した選好を形成してしまい，幸福になるために通常は必要だと思われる選好を持たなくなる可能性がある。
これを適応的選好の形成というが，選好充足だけで幸福を計ることが問題なのはこのためである。
選好功利主義の目的は，人々の持つ選好を最大限満たすことであるが，その場合，限定された選好を持つ人々をたくさん作り，それを満たせば目的は達せられたことになる。
しかしこれで万事解決だと思う人は少ないだろう。

### 愚かな選好を充足すべきか

類似の問題として，われわれが持つ「愚かな選好」をどう考えるかという問題もある。

たとえば，交通事故の悲惨さをよく知っていれば，シートベルトをきちんと締めるかもしれないが，実際にはつい面倒くさいと締めないですませることもある。
この場合，

1. 現に持っている「面倒だからシートベルトを締めない」という選好を充足すべきか
2. 「事故が起きた場合に悲惨なことにならぬようシートベルトを締める」という，よく考えれば持ったはずの選好を充足すべきか

という問題が出てくる。
2.は選好充足に一種の合理性の条件を入れる発想と見ることができる。
この発想は二つ問題がある。

1. パターナリスティックな理論になる危険があるということ。
2. この発想は現に持つ選好を充足するという元々の発想から遠く離れており，もはや選好という言葉を使う必要すらないのではないかということ。

### 幸福＝利益を充足させることか

上で満たように，当人が現に抱く選好を充足することは必ずしも幸福につながらない。
そこで，本人の選好よりも利益やニーズを満たすことが幸福につながるという考え方が出てくる。
この場合の利益やニーズは本人の意思とは基本的に関係なく，客観的に決まるとされる。
この立場は「厚生功利主義」と呼ばれることがある。

この立場の大きな利点は，「効用の個人間比較」の問題を回避できることだ。
つまり，「わたしの快楽や選好の強さと，あなたの快楽や選好の強さをどうやって比較できるのか」という問題である。
利益やニーズは快苦や選好よりも客観的なものであり，人間である限り等しく幸福に役立つものと考えられるのだ。

この立場の最大の問題は，人々の利益について真に客観的なリストを作るのが難しいということだ。
もっとも，この考え方は政治レベルではかなりうまく行くだろう。
人間の幸福につながる利益のリストに比して，不利益のリストは合意が得やすい。

このように，利益や不利益の客観的リストを作るという発想は，ある程度までは魅力的である。
とはいえ，幸福論としては次のような問題が残る。
この立場は，幸福になるための基盤を提供するだけで，「幸福とは何か」という根本的な問いに答えていない。

### 筆者の暫定的な見解

まず，政治のレベルでは，政府は人々が不幸になる原因を取り除くことを重視すべきだろう。
一般に政府は，個々人がどのような幸福観を抱いているのかを正確に知ることはできない。
そこで，人々に共通する最小公約数[原文ママ]と言える利益のリストないし不利益のリストを作って，幸福への基盤を提供することが重要だと考える。

功利主義はすべての人々の幸福を等しく考慮に入れるが，政府の主要な役割は不幸を減ずることだと考えるなら，社会的弱者を重点的に助けるような政策を支持することになるだろう。
あとはベンタムやミルの考えていたように，諸個人の自由な活動に任せることが，最大多数の最大幸福につながると考えられる。

次に，個人のレベルで考えれば，通常は自分が持っている欲求を満たすことが幸福につながるだろう。
ただ，選好充足の話のところで見たように，現に持っている欲求を満たすことが常に幸福につながるとは限らない。
そこでわれわれは，落ち着いて考えられるときに生き方を反省し，現に持つ欲求を吟味する必要があるだろう。
欲求をふるいにかけ，全体として合理的な欲求のセットを持つことが幸福への近道だと思われる。

## 第7章 道徳心理学と功利主義

### なぜわれわれは援助しないのか

今日の世界には，飢えや病気に苦しむ大勢の人がいて，先進国に住むわれわれの多くは報道を通じてそのことを知っている。
われわれの多くはこのような状況について良心の痛みを感じることがあるだろうが，実際のところほとんど何の援助もしない。
それはなぜだろうか。
最大多数の最大幸福を信条とする功利主義者にとってこと問いはとても重要だ。

一般にこのような，「人間はどのように考え，行動しているか」という問いは，心理学のような実証科学の問いだといえる。
そこで，まずこの問いについて最近の心理学や脳科学の議論を見てみる。

### 特定の人の命と統計上の命

オレゴン大学心理学教授のポール・スロヴィックはマザー・テレサの言葉を受けて「『群集を見てもわたしは決して助けようとしない』――心理的麻痺と虐殺」というタイトルの論文を二〇〇七年に発表した。

スロヴィックは世界中で貧困や戦争で苦しんでいる人々について，多くの例を挙げる。
しかし先進国に住むわれわれは，統計上の数字を目にしても何らかの援助行為に結実することはほとんどない。

目の前に困っている人がいれば可能な範囲で助けようとするが，援助を必要とする人の数が増え，それが統計的な数になると，われわれはしばしば「心理的麻痺」に陥る。
スロヴィックは，われわれが大規模な災害や虐殺に対してほとんど何も感じないのは，人間の心理には何か根本的な欠陥があるからではないかと考えたのだ。

そこで，スロヴィックは米国の大学生を対象に次のような研究を行なった。
学生たちは三つのグループに分けられ，アフリカで飢餓に苦しむ子どもに対して，五ドル以内でいくら寄付するかを判断するよう求められた。
グループ1には特定の個人の人命を救うためにいくら寄付する気があるかを尋ね，グループ2には統計上の人命を救うためにいくら寄付する気があるかを尋ね，グループ3には特定個人の人命と統計上の人命の両方について同じ質問をした。

すると，以下のように興味深い結果が得られた。
グループ1の寄付の平均額は，グループ2よりも多く，さらにグループ3よりも多かった(グループ2と3の間には統計的な有意差はなかった)。
また，共感の度合いについても，特定個人の人命が問題になる場合の方が，人々の共感の度合いが強くなることが示唆された。
スロヴィックは同種の他の研究も引いて，われわれには統計的な人命よりも特定の人の命を助けようとする傾向があると論じている。

統計上の人命が問題になる大規模な災害や虐殺などでは，われわれの理性的な判断とわれわれの共感に基づく判断の間には，乖離が生じる可能性がある。
だが，なぜこのような事態が生じてしまうのか。
スロヴィックが考えたように，何か根本的な欠陥がわれわれ人間にはあるのだろうか。

### 経験的思考と分析的思考

このような乖離を説明するためにしばしば引き合いに出されるのが，近年心理学や脳科学の分野で盛んに論じられている，思考には二つのシステムがあるとする理論だ。
マサチューセッツ大学の心理学名誉教授のエプスタインは，われわれの思考様態を経験的システム(直感的思考)と分析的システム(合理的思考)に分け，次の表のように対比的に説明する。

システム1: 経験的システム             |システム2: 分析的システム
---------------------------------|--------------------------------
情動的: 快苦指向                    |倫理的: 理性指向
連想による結合                      |倫理的評価による結合
過去の経験に伴う感情に動機付けられた行動  |出来事の自覚的評価に動機付けられた行動
現実をイメージ，比喩，物語によって記号化  |現実を抽象的シンボル，言葉，数字で記号化
より速い処理: 直ちに行動するのに向く     |より遅い処理: 少し時間を置いた行動に向く
正しさは自明: 「経験することは信じること」|倫理と証拠による正当化が必要

こうした二つの思考システムの議論を踏まえて，スロヴィックは先の論文で，「心理的麻痺」が起きる仕組みについて次のように考えた。
すなわち，特定の人の命が問題になる事例では，直感的な経験的システムが強く働くが，統計上の人命が問題になる事例では，経験的システムがそれほど強くは働かないため，両システム間で判断に乖離が生じ，その結果「心理的麻痺」という現象が起きる，というのがスロヴィックの考えである。
二つのシステムについては心理学者の間でもまだ多くの議論がある。
とはいえ，この理論は，スロヴィックらの研究結果を理解するうえで，一つの有効な概念枠組みを提供していると言える。

### 記述理論と規範理論の関係

ここで，このような心理学や脳科学の研究成果と倫理学との関係を少し考えておこう。
脳科学や心理学は「人間はこう考え・行動する」という人間の正確な記述を目指すという意味で，基本的に記述理論ないし実証理論だ。
それに対して倫理学は「人間はこう考え・行動すべきだ」という当為を問題にしているため，規範理論と呼ばれる。

記述理論は，規範理論にとって大変重要な意味を持つ。
規範理論を作る際に，記述理論の知見を取り入れる必要があるからだ。

実際，倫理学においては昔から，人間本性についての理解を問題としてきた。
現在のわれわれにとって問題となるのは，現代の心理学や脳科学といった記述理論の知見に照らして，どういう規範理論を作るべきかということだ。

倫理的問題に関する記述的な研究と規範理論の関係を考えるうえで興味深いのは，グリーンらの研究だ。
[トロリー問題](https://ja.wikipedia.org/wiki/%E3%83%88%E3%83%AD%E3%83%83%E3%82%B3%E5%95%8F%E9%A1%8C)のようなこれまで英米倫理学でよく知られていた思考実験を数多く用いて，人々が倫理的判断を下すさいの脳活動をfMRIで測定している。
グリーンらは分岐器を動かすケースに比べて突き落とすケースは直接手を下す感じが強いことから，前者を「非個人的」なケース，後者を「個人的」なケースと呼んだ。
そして，fMRIを用いて脳の活動を調べた結果，前者よりも後者の方が，感情を司る脳の部位が活性化することが見出された。
逆に，「個人的」なケースに関して直観に反すると思われる答えを出した人は，認知的活動を司る脳の部位が他の人に比べてより活性化しており，また答えるのに時間がかかった。
グリーンらの考えによれば，心理的抵抗を克服するのに時間を要したためである。
別の論文では，この心理的葛藤を，われわれが多かれ少なかれ有している功利主義的な思考と非功利主義的な思考の間で生じる葛藤だと論じている。

このような脳科学の研究は，特定の規範理論を直ちに否定するのもではないが，倫理における情動の役割を真剣に考えることをわれわれに要求しているように思われる。
功利主義は道徳的思考における理性の役割を重視し，情動や感情を道徳の基礎とみなすことを拒否する。
しかし，グリーンらの研究や，今日の道徳心理学の研究が示唆するのは，道徳的思考における感情の役割はそう簡単には退けられないということだ。
大雑把に言えば，功利主義は理性を重視する合理主義的な規範理論であるのに対して，今日の記述理論が新しい人間理解として示すのは，われわれの行動は往々にして共感その他の情動的な反応に基づいているということだ。
その人間理解が正しいとすると，道徳について考える際，理性だけでなく，必ずしも理性と一致しない感情の役割についても検討しなければ，十分な規範理論を作ることができないことになろう。

### 「直観的思考の強化」戦略

実はこの問題は，ミルが『功利主義論』で論じている古典的な問題に通じている。
それは，功利主義が支持する結論が，直観に反するか，直観的に自明でない場合に，その結論に従うために必要な動機はどのようなものであるか，という問題である。
以下ではミルやスロヴィックの議論も参考にしながら，援助義務に対する動機を生み出す方法について，具体的な三つの戦略を考えてみる。

第一の戦略は，教育や文化の力によって海外援助に関する直観的思考を強化することだ。
たしかに共感能力の陶冶は倫理における重要な課題であり，実際のところ，ミルの時代より今日のわれわれの共感は国境や人種の壁を越えて広がっているだろう。
しかし，スロヴィックの研究が示唆するのは，それが七〇億人を超える人々が住むこの世界のすみずみに及ぶほど強くはないということである。
また，持続的に他人の境遇に共感し続けることも難しいように思われる。
したがって，「共感能力を高めよ」という教えは，統計上の人命に対する共感能力の限界という壁に突き当たらざるをえない。

### 「共感能力の特性利用」戦略

第二の戦略は，直観的思考の特性を利用するものだ。
リバタリアン・パターナリズムはこの戦略と発想が近い。
すなわち，人々を取り巻く環境を変更することによって，人々が理性を用いて意識的に選択したり誰かに強制されたりしなくても，自然と正しい選択肢を選べるようにしようとする発想だ。

しかし，この戦略にもいくつか問題があるように思われる。
まず，われわれの共感能力の特性を利用した戦略を取る場合，人々の生活を想像しにくい遠い国々よりも，想像しやすい時刻や近隣諸国の災害の被災者が優先され，本当に支援が必要な必要なところに関心が向かない可能性がある。
第二の戦略は，きっかけ作りとしては有効だろうが，情報に対して批判的に考える習慣なしには，合理的思考によって支持される援助活動につながらない可能性がある。
その意味で，メディア・リテラシー教育への目配りも必要だろう。

次に，第二の戦略によって人々を本当に意図した方向に導くことができるのかという問題もある。
第二の戦略はいわば目をつぶっている人々の手を引いてやることで，望ましいゴールに誘導してやろうというものだ。
このような発想は人々を子ども扱いしていると批判されるかもしれず，人間の非合理性を考えれば，結局この戦略が一番合理的であると説得し，そのような扱いに同意してもらう必要がある。
しかし，そうすると感情だけでなく理性にも訴えなければならないことになるだろう。
われわれは自分たちの理性の限界を意識した戦略を立てると同時に，それが用いられることに理解し同意していないといけないというわけだ。
これがどれ程うまく行くのかは，今後の実践における検討課題となろう。

### 「理性的思考の義務付け」戦略

第三の戦略は，直観的思考よりもあくまで合理的思考を重視し，理性的な判断に従って行為することを自らに義務付けるという戦略だ。
しかし，この戦略にもいくつかの問題がある。
まず，道徳における合理的思考を発達させるためにどのような教育を施せばよいのかについて検討する必要がある。
また，教育すれば誰もがこのような思考を身に付けられるのかも問題になる。
それに加えて，直観的思考による判断が不在のところで合理的思考に従う場合，その判断の正しさについてかなり慎重になる必要がある。
直観的思考は必ずしも常に正しいとは限らないが，倫理的問題に対してわれわれが抱く感情は，ちゃんと教育を受けていれば，ある程度までは正しい可能性が高いだろう。
したがって，直観的思考と異なる仕方で行動する場合は，判断を大きく過つ可能性があることを意識しておかなければならない。

とはいえ，世界の飢餓に苦しむ人々を助けるためには，われわれは共感に基づく直観的思考を強化・利用するだけではおそらく十分ではないと筆者は考える。
倫理について合理的な思考をある程度陶冶することも重要になると思われる。
倫理学では，共感が届かないところに援助する必要はないとするような議論も存在するが，その正しさを議論して確かめるのにも，倫理について合理的に思考できる必要があるのだ。

### 倫理学から実践へ

現実の社会には多くの解決困難な問題が山積している。
本章では，われわれの倫理的思考に関係する心理学や脳科学の最近の研究を紹介し，現在のわれわれの倫理についての思考が，こうした社会問題に対応するのに十分でないかもしれないことを示唆した。
倫理学を学んですぐに現実の社会問題を解決できるわけではない。
しかし，倫理学を学ぶことは，そのために必要なものを反省する機会となり，社会問題を批判的に検討する能力を身に付けることにつながるだろう。
われわれは倫理について知っているようでいて，あまり知らないのかもしれない。
まず，倫理とは何なのか，また，われわれの倫理的思考はどのようなものなのかを考えることも大事なのだ。
