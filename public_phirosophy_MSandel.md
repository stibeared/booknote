# マイケル・サンデル『公共哲学: 政治における道徳を考える』(2011)

## 負荷なき自己

リベラルは選択の自由を支持するために，許容と賞賛の区別を説く。
ではなぜ他の重要な価値観ではなく，寛容と選択の自由なのだろうか。
その答えにある種の道徳的相対主義が含まれていることがある。
すなわち，あらゆる道徳的価値観は主観にすぎないということだ。
しかし，これではリベラルの価値観(寛容と選択の自由)すら相対化されてしまう。

ではどう答えるか。
最近の政治哲学は二つの答えを提示する。
一つは功利主義である。
つまり全体の幸福の最大化によってリベラルの原理を擁護する。
これには多くの批判があるが，最近の論争では，功利主義が個人の権利の尊重といったリベラルな原理に説得力ある基盤を提供しないのではないかという批判がある。
すなわち，多数者の幸福のために少数者の幸福が抑圧される場合があるということである。
もちろんこれに対して，個人の権利の尊重をすることは，長期的には効用の増大につながるという反論がある。
しかしこうした計算はあやふやで根拠に乏しい。
多数派の意思がそれ自体では個人の権利を保証しないとすれば，それはリベラルの原理の基盤にはふさわしくない。

二つめはカント哲学である。
カントは効用のような経験的原理は道徳律の基盤には適さないと主張した。
役に立つからという理由だけで自由と権利を擁護すると，権利が脆弱になるだけでなく，人間に固有の尊厳に敬意が払われなくなると批判する。
現代のリベラルはカントの主張を拡大し，功利主義がそれぞれの人間の違い，多元性と個性を尊重しないと主張する。
つまり，一部の人間を全体の幸福の手段として利用し，一人ひとりの人間が目的として扱われていないのである。

ではカント主義リベラルは，果たしてどう善についての特定の概念に頼らずに権利を説明するのか。
彼らが提案するのは，「正」と「善」の区別である。
つまり，国家が公正な枠組み(基本的な権利や自由の枠組み)を支持することと，ある特定の目的を肯定することは違うと主張するのだ。

さて，目的に中立な枠組みへの志向というのは，一種の価値観である。
この点でカント主義リベラルは相対主義ではないのだが，その価値観とはまさに，望ましい生き方や善の概念を肯定しないところにある。
すると，カント主義リベラルは二つの意味で正(権利，right)が善より優先されることになる。
第一に，個人の権利は全体の善のために犠牲にされてはならない。
第二に，そうした権利を規定する正義の原理は，善き生に関する特定の見解を前提にしてはならない。

権利に基づく倫理とそれによって具体化された人間の概念は，功利主義との対決によって大部分が形づくられてきた。
すなわち，功利主義者がわれわれの多くの欲求をたった一つの欲求の体系にまとめるのに対し，カント主義者は人間の個別性を強調する。
功利主義的な自己が欲求の総計と定義されるだけなのに対し，カント主義的な自己は選択する自己であり，それがつねに持っている欲求や目的からは独立した存在である。
権利に基づく倫理にとって，われわれは本質的にばらばらで独立した自己であるがゆえに，中立の枠組みを必要とする。
自己が目的に先立つゆえに，正(権利)は善に先立つのである。

この負荷なき自己の人間観は，カント主義リベラルの理論に説得力を与えるが，またその破綻によって弱点となるのだ。

カントによれば，われわれが決定論的でまったく経験的な存在であれば，自律的主体としての自由を持たない。
であれば，われわれが自由であると仮定する限り，独立で第一の原因である主体が存在するのである。
そしてその自律的主体こそ負荷なき自己であり，あらゆる経験的目的に優先する(定言的)ために正の基盤たりうるのである。

さて，英米哲学の伝統において，このような超越論的主体は奇妙である。
そこでロールズはこれを受け入れずとも，権利を重んじて正義の卓越性を肯定できることを示すのである。
このとき利用されるのが原初状態である。

原初状態とは要するに，自分がどんな人間かを知らず，みずからの興味や目的，善の概念すら知らないときに，社会を支配する原理としてどんな原理を選ぶかを想像させる。
このとき選ばれるのが正義の原理である。
しかも，こうした原初状態が有効に機能すれば，それはいかなる特定の目的も前提しない原理なのだ。

(以下メモ的)

### 格差原理について

負荷なき自己であること，それは私についてのすべてに当てはまる。
それゆえに私は個人としてはあらゆることにいっさいふさわしくないのだ。
正の優先と，功績の否定と負荷なき自己が見事につじつまがあっている。

しかし格差原理はさらに多くを求め，破綻にいたる。
格差原理の出発点は，資産がたまたま私のものであるにすぎないという，負荷なき自己と相性のよい考えだ。
だが，その終着点は，よってその資産は共有財産で，それを行使して得たものについては社会が優先権を持つという仮定だ。
だがこの仮定には正当な理由がない。
社会の領域，ついでにいえば人類の領域のなかにその資産が占める位置は，道徳的観点から見て恣意性が低いと考える理由はどこにもない。
そして，その資産が私のなかの恣意性のせいで私の目的に役立てるのに適さないとすれば，特定の社会のなかの恣意性のせいで，その社会の目的に役立てるのに適するという明白な理由もない。
すなわち，格差原理は功利主義と同様，分かち合いの原理であり，その前提として，この原理によってみずからの資産が配分される人びとや，みずからの尽力が集団の努力に組み入れられる人びとのあいだには，あらかじめ何らかの道徳的絆がなければならない。
さもなくば，格差原理はある人びとをほかの人びとの目的達成の手段として利用するための方法にすぎなくなる。

だが，コミュニティの協同的ビジョンだけではこのような原理の道徳的基盤がわからない。
構成的概念なしでは，正当化できない。
格差原理が必要としながら提供できないものは私の持つ資産を共有するのがふさわしいと思える人びとを見分ける方法であり，そもそもわれわれ自身が相互に恩を受け，道徳的にかかわっていることを理解する方法である。

## 二種類のコミュニタリアン

カントやロールズにおける善に対する正の優先が意味するのは二つである。
一つめは，ある種の個人的権利は非常に重要で，公共の福祉ですらそれを踏みにじることは許されない。
二つめは，われわれの権利を規定する正義の原理の正当性は，善き生をめぐる特定の構想に依拠しない。
サンデルが『リベラリズムと正義の限界』で異議を唱えんとするのは二つめである。

正義と善が相関しており，独立した存在ではないとする考え方はコミュニタリアンに共通しているが，この主張には二種類ある。
一つめは，正義の原理はその道徳的な力を，特定のコミュニティや伝統のなかで一般に支持されていたり，広く共有されていたりする価値観から引き出すのだとする。
この見解によれば，正を認定する論拠は，そうした正が当の伝統やコミュニティを特徴づける共通理解に含まれていることを示すところにある。
これは，何が正義で何が正義でないかを定義するのはコミュニティの価値観であるという意味で，コミュニタリアン的だ。
二つめは，正義の原理は，それが資する目的の道徳的価値や内在的善に応じて正当化されるとする。
この見解によれば，正を認定する論拠は，それが重要な人間的善を称賛したり促進したりすることを示すところにある。
これは，こうした善がコミュニティの価値観であるかどうかは関係ない点で，厳密にはコミュニタリアン的ではない。

このうち，第一のものは適切ではない。
何らかの慣習が特定のコミュニティで認められているという事実は，それを正義とする十分条件にはならない。
権利を擁護する論拠は本質的な道徳・宗教上の教説に中立であるべきというリベラル派と，権利は支配的な社会的価値を土台とすべきというコミュニタリアンは，同様の過ちを犯している。
どちらも，権利を促進する目的の内容について判断を避けようとしている。

## 政治的リベラリズム

善に対する正の優先を擁護するには二つの方法がある。
一つは，カント的な人格概念を擁護することによって。
もう一つは，カント的な概念から離れることによって。
ロールズが『政治的リベラリズム』で取ったのは後者の方法だった。

ロールズは，リベラリズムを支持する論拠は政治的であり，哲学的でも形而上学的でもないから，自己の本性をめぐる賛否両論ある主張には左右されないと論ずる。
善に対する正の優先は，現代の民主主義社会に住む人びとは善についてたいてい意見が合わないという事実への現実的な対策なのだ。
つまりここで，ロールズは政治的リベラリズムと包括的リベラリズムを区別する。
後者においては，自律，個性，独立独歩といった一定の道徳的理想の名において，リベラルな政治制度が肯定される。
一方前者は，自己の概念をめぐる論争を含む，包括的な教説から生じる道徳・宗教上の論争においてどちらか一方に味方することはない。

政治的リベラリズムは，正義の原理の哲学的根拠を追い求めるのではなく，「重なり合うコンセンサス」の支えを追い求める。
すなわち，さまざまな人びとがさまざまな理由から，平等な基本的自由といったリベラルな政治制度を納得のうえで認めることがありうるということだ。
しかし，カント的な人格概念への依拠を放棄するなら，原初状態をどのようにして正当化できるのか。
社会の基本構造を律する正義の原理を，人間の最高の目的に関する最善の理解に基づけるべきでないのはどうしてだろうか。

政治的リベラリズムの答えはこうである。
目的を捨象した人格の観点から正義について考えなければならない理由は，こうした手続きが，目的に優先する自由で独立した自己としてのわれわれの本性を表現するということではない。
正義に関するこうした考え方を正当化するのは，次のような事実である。
つまり，われわれはみずからを，必ずしもすべての道徳的な目的にとってではなく政治的な目的にとって，過去の義務や責務に縛られない自由で独立した市民だとみなすべきなのである。
政治的リベラリズムにとって，この「人格の政治的構想」によって原初状態の企図は正当化される。
人格の政治的構想の，カント的な人格の構想との重要な違いは，その適用範囲がわれわれの公的なアイデンティティ，市民としてのアイデンティティに限られる点である。
この人格の政治的構想は，われわれが市民としてなす主張を，どんなものであれわれわれがそれを主張したというだけの理由で重要だとする(自己認証的)。
これゆえ，政治的リベラリズムによれば，われわれが目的から切り離された立場で原初状態が促すような仕方で正義について省察しなければならないのだ。

しかし，そもそも，人格の政治的構想の観点に立つ必要があるのはなぜか。
市民としてのアイデンティティと，より広く考えられた道徳的人格としてのアイデンティティを分けることに，なぜこだわるのか。
ロールズによれば，市民としてのアイデンティティと人格としてのアイデンティティのこうした分離，すなわち「二元性」は，「民主的な政治文化の特殊な本性に由来する」という。
仮にこれが正しく，政治文化にリベラルな自己像が内在するとしても，だからといって，その自己像を肯定し，それが支持する正義の構想を受け入れる十分な根拠が手に入るだろうか。
公正としての正義は，「公共文化そのものを，暗黙のうちに認識された基本的な考えや原理の共有資産とみなすこと」から始まるものの，広く共有されているからという根拠だけで，これらの原理を肯定するわけではない。
ロールズは，自分の正義の原理は重なり合うコンセンサスの支持を得られると主張するが，彼の追求する重なり合うコンセンサスとは「単なる利害調整ではなく」，対立する見解の妥協点でもないのだ。
さまざまな道徳的・宗教的構想の信奉者たちは，自分の信じる構想のなかから引き出した理由によって，正義の原理を承認することから始める。
だが，万事が順調に進めば，これらの原理を重要な政治的価値観を表すものとして支持するようになる。
人びとは，リベラルな制度の支配する多元的な社会で生きるようになると，リベラルな原理への傾倒を強める美徳を身につけるのだ。

ロールズはこう強調する。リベラルな美徳をすばらしい公共善として支持し，その涵養を促すことは，包括的な道徳概念に基づく完全主義の国家を是認することとは違う，と。
それは，善に対する正の優先と矛盾しないのだ。
というのも，政治的リベラリズムが支持するリベラルな美徳は，政治的な目的しか持たないからだ。

『政治的リベラリズム』が自我の本質をめぐる論争から正の優先性を救い出すには，その代償として，その他の面で正の優先性を無防備にせざるをえない。
具体的には三つの反論にさらされている。

第一に，ロールズの訴える「政治的価値観」は大切だとしても，道徳・宗教上の包括的な教説から提起される主張を，政治的な目的のためにカッコに入れる，つまり脇へ置くことが必ずしも合理的だとはかぎらない。
重大な道徳的問題がかかわっている場合，政治的合意のために道徳的・宗教的論争をカッコに入れることが合理的かどうかは，対立する道徳的・宗教的教説のどれが正しいのかにある程度かかっているのだ。

第二に，政治的リベラリズムにおいて，善に対する正の優先の擁護論が依拠するのは，現代の民主主義社会が善に関する「理にかなった多元主義の事実」を特徴としているという主張である。
現代の民主主義社会に住む人びとが，相容れない多様な道徳的・宗教的見解を持っているのというのは確かにそのとおりだが，道徳や宗教に関する「理にかなった多元主義の事実」が正義の問題には当てはまらないとは言えない。

第三に，政治的リベラリズムが掲げる公共的理性の理想によれば，市民が政治や憲法の基本問題を論じる際，自分の信じる道徳・宗教上の理念を持ち出すのは正当ではないという。
だが，これはあまりにも厳しい制約であり，政治論議を貧弱にし，公共の熟議の重要な要素を排除してしまう。

少なくとも重大な道徳的問題にかかわる場合，正義の政治的構想は，カッコに入れると称する道徳問題への答えを前提せざるをえなくなる。
つまり，善に対する正の優先が維持できなくなる。
政治的リベラリズムが抱えるいっそうの困難は，そもそも善に対する正の優先を主張する理由にかかわっている。
政治的リベラリズムにとって，正と善の非対称性の土台をなすのはカント的な人格構想ではなく，現代の民主主義のある種の特徴である。
すなわち「理にかなった多元主義の事実」である。
道徳や宗教の問題に関して，自由な体制の下で人間の理性を行使した正常な結果としての多元主義である。

しかし，仮にその事実が正しいとしても，正と善の非対称性はさらに別の前提に依拠している。
すなわち，正義に関しては同様の「理にかなった多元主義の事実」が存在しないという前提である。
だが果たしてそれは存在しないのだろうか。

政治的リベラル派は，正義をめぐる意見の不一致を二種類に区別することで，こうした問いに答えるかもしれない。
正義の諸原理はいかなるものであるべきかについての意見の不一致と，それらの原理がどう適用されるべきかについての意見の不一致があるというのだ。
ロールズのようなリベラル派平等主義者とノージックやフリードマンのようなリバタリアンと間には，配分的正義の正しい原理とは何かをめぐる意見の不一致があり，これは正義の適用をめぐる意見の不一致ではない。

政治的リベラリズムはこの反論に対して答えがないわけではないが，ここで出さざるをえない答えは，政治的リベラリズムがほかの場面で持ち出す寛容の精神から逸脱するものだ。
配分的正義についても多元主義の事実はあるが，理にかなった多元主義の事実はないとロールズは答えるに違いない。
ロールズは正当化を，原理と考え抜かれた判断とのあいだの，「内省的均衡」を目指す相互調整プロセスとみなすことによって，格差原理はリバタリアンが提示する選択肢よりも理にかなっていることを示そうとする。
だが，それならば，道徳的・宗教的論争の場合にも同様の内省が可能ではないとどうして言い切れるのだろうか。
正と同じく善についても論理的に考えられるなら，正と善は非対照的だとする政治的リベラリズムの主張は崩れることになる。

政治的リベラリズムは，政治論議へ真に貢献するような議論，とりわけ憲法の本質的要素や，基本的な正義に関する議論に厳しい制約を課す。
こうした制約は，善に対する正の優先を反映するもので，「公共的理性の理念」によって求められているとロールズはいう。
民主主義社会の市民は，包括的な道徳・宗教上の概念を共有していないので，公共的理性はそうした概念に言及すべきではないのだ。

公共的理性の制約を正当化するのは，それによって可能となる政治的価値――たとえば礼節や相互尊重――である。
制約的な公共的理性を評価するには，その道徳・政治上の代償と，それが可能にするという政治的価値とを秤にかける必要がある。
同時に，こうした政治的価値は，公共的理性の制約を緩めても実現できるかどうかを問わなければならない。

リベラルな公共的理性の代償には二つの種類がある。
厳密に道徳的な代償は，リベラルな公共的理性が，正義の問題について判断する際に脇へ置くようわれわれに求める教説の妥当性と重要性によって決まる。
代償が最も大きくなるのは，正義の政治的構想が，重大な悪徳を寛容に扱うことを認めるときだ。
そして道徳的な代償よりもさらに大きいのが，政治的代償だ。
つまり，公共的理性のビジョンがあまりにも貧弱なため，活力ある民主的生活の道徳的エネルギーを取り込めず，道徳的な空白が生まれ，不寛容な道徳主義や見当違いでくだらないその他の道徳主義を招き寄せてしまう。

リベラルな公共的理性は制約が厳しすぎるとすれば，もっとふところの広い公共的理性によって，政治的リベラリズムが促進しようとする理想が犠牲になかどうかを問わなければならない。
とりわけ，対立する道徳観や宗教観を持つ市民のあいだの相互尊重が問題だ。
リベラル派は，同胞市民の信念を尊重すべく，それらを正義をめぐる政治論議に持ち込まないように要求する。
だがこれは，相互尊重を理解する唯一の方法ではないし，最適な方法ですらないかもしれない。
熟議型の考え方によれば，われわれは道徳や宗教をめぐる同胞市民の信念を尊重すべく，関与あるいは留意，ときには批判し異議をとなえ，ときには耳を傾けてそこから学ぶのだ。
熟議や関与を通じた相互尊重は，リベラリズムが認めるものよりふところの広い公共的理性を提供し，また，多元的社会によりふさわしいものでもある。
道徳や宗教をめぐるわれわれの意見の不一致が，人間的善の究極の多様性を反映するものであるかぎり，熟議型の相互尊重を通じて，われわれは多様な生が表現する固有の善をよりよく理解できるようになるだろう。
